FROM node:alpine
LABEL name=semaphore-test version=1.0.0

COPY package.json package.json
RUN yarn --production
COPY . .

EXPOSE 3000

CMD ["yarn", "start"]
