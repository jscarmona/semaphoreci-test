const http = require('http');

const getCurrentTime = () => new Date().getTime();

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.write(JSON.stringify({
    ack: getCurrentTime()
  }));
  res.end()
});

server.listen(3000);
